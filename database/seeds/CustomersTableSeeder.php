<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
             'name' => 's',
             'email'=>'s@s.com',
             'phone'=>'0545949779',
            'user_id'=>'1',
           
        ],
        [
            'name' => 'g',
            'email'=>'g@g.com',
           'phone'=>'0545949759',
           'user_id'=>'1',
       ],
        
        ]);

    }
}
