<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Gate;
use\Illuminate\Support\Facades\Auth;
use App\Customer;
 use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$id=Auth::id();
       // $customers = User::find($id)->customers;
      
        $customers=Customer::all();
        $users=User::all();
        return view("customers.index",['customers'=>$customers,'users'=>$users]);

    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("customers.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer=new Customer();
        $id=Auth::id();
        $customer->name=$request->name;
        $customer->email=$request->email;
        $customer->phone=$request->phone;
        $customer->user_id=$id;
        $customer->save();
        return redirect('customers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer=Customer::find($id);
        return view("customers.edit",['customer'=>$customer]);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    
    {
        if (Gate::denies('manager')) {
        
            abort(403,"Are you a hacker or what?");

       } 

        $customer=Customer::find($id);
        $customer->update($request->all());
        
        $customer->update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success','status'=>$request->status),200);
        }
        return redirect ('customers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
        
            abort(403,"Are you a hacker or what?");

       } 

        $customer=Customer::find($id);
        $customer->delete();
        return redirect ('customers');

    }
}
